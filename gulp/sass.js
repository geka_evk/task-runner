
const gulp      = require('gulp'),
      conf      = require('./_conf'),
      sass      = require('gulp-sass'),
      sourcemap = require('gulp-sourcemaps'),
      concat    = require('gulp-concat'),
      rename    = require('gulp-rename');


gulp.task('sass', () => {
    return gulp.src( conf.src.sass )
        .pipe( sourcemap.init() )
        .pipe( sass({outputStyle: 'expanded'}).on('error', sass.logError) )
        .pipe( concat(conf.files.css) )
        .pipe( gulp.dest(conf.dist.css) )
        .pipe( sass({outputStyle: 'compressed'}) )
        .pipe( rename({suffix: '.min'}) )
        .pipe( sourcemap.write('.') )
        .pipe( gulp.dest(conf.dist.css) )
        .pipe( conf.server.reload({stream: true}) );
});
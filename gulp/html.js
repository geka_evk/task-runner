
const gulp = require('gulp'),
      conf = require('./_conf');


gulp.task('html', () => {
    return gulp.src( conf.src.html )
        .pipe( gulp.dest( conf.dist.html ) )
        .pipe( conf.server.reload({stream: true}) );
});
    
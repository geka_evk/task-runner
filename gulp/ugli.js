'use strict';

const gulp    = require('gulp'),
      through = require('through2');
      

module.exports =  () => {
    return through.obj( (chunk, enc, cb) => {     
        
        let arr = chunk.contents.toString().split(/\r?\n+/),    // разбиваю весь код на массив строк
            isQ1,       // начата строка с '
            isQ2,       // начата строка с "
            isQ3,       // начата строка с `
            isMC,       // открыт многострочный комментарий
            k, j;       // позиция начала многострочного коммента (строка, позиция в строке) 

        const outStr = () => ( !(isQ1 || isQ2 || isQ3) );

        arr.forEach( (str, inx, mas) => {
            for (let i = 0, len = str.length-1; i < len; i++) {
                
                if (str[i] === '\'' && !isMC)  isQ1 = !isQ1;
                if (str[i] === '"'  && !isMC)  isQ2 = !isQ2;
                if (str[i] === '`'  && !isMC)  isQ3 = !isQ3;

                if ( str.substr(i, 2) === '//' && outStr() && !isMC )  {
                    mas[inx] = str.slice(0, i);
                    break;
                }

                if ( str.substr(i, 2) === '/*' && outStr() ) {
                    isMC = true;
                    k = inx; //  запоминаю строку начала многострочного коммента
                    j = i;   //  запоминаю позицию (в строке) начала многострочного коммента
                }

                if ( str.substr(i, 2) === '*/' && isMC ) {
                    isMC = false;
                    mas[inx] = (inx === k) ? str.replace(str.substr(j, i+2 - j), '')  
                                           : str.slice(i + 2);
                    i--;
                    break;
                }
            } // end of  for

            if (isMC) {
                mas[ inx ] = j===0 ? '' : mas[ inx ].slice(0, j);
                j = 0;
            }

        } );  // end of  forEach
        
        arr = arr       // разбиваю код на отдельные строки, убирая пустые
            .filter( el => el !== '' )
            .join('\n');
        
        chunk.contents = new Buffer(arr, "utf-8");

        cb( null, chunk );
    } );
}



const gulp      = require('gulp'),
      conf      = require('./_conf'),
      eslint    = require('gulp-eslint'),
      sourcemap = require('gulp-sourcemaps'),
      babel     = require('gulp-babel'),
      concat    = require('gulp-concat'),
      // jsmin     = require('gulp-jsmin'),
      rename    = require('gulp-rename'),
      jsFiles   = require('main-bower-files'),
      ugli      = require('./ugli'); // мой плагин для аглификации


gulp.task('js:eslint', () => {
    return gulp.src( conf.src.js ) 
        .pipe( eslint('gulp/.eslintrc.json') )
        .pipe( eslint.format() )
        .pipe( eslint.failAfterError() );
})

gulp.task('js', ['js:eslint'],  () => {
    return gulp.src( conf.src.js )
        .pipe( sourcemap.init() )
        .pipe( babel({
            presets: ['es2015']
        }) )
        .pipe( concat(conf.files.js) )
        .pipe( gulp.dest(conf.dist.js) )
        // .pipe( jsmin() )
        // .pipe( rename({suffix: '.min'}) )
        // .pipe( gulp.dest(conf.dist.js) )
        .pipe( ugli() )
        .pipe( rename({suffix: '.ugli'}) )
        .pipe( sourcemap.write('.') )
        .pipe( gulp.dest(conf.dist.js) )
        .pipe( conf.server.reload({stream: true}) );
});

gulp.task('jsFiles', () => {    //  copy mainJS-files from dependencies in bower.json
    return gulp.src( jsFiles() )
        .pipe( gulp.dest(conf.dist.libs) );
});


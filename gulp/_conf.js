"use stict";

const bs = require('browser-sync').create();

module.exports = {     
    src: {
        js:   'src/**/*.js',
        sass: 'src/sass/**/*.{scss,css}',
        html: 'src/index.html'
    },
    dist: {
        js:   'dist/js',
        css:  'dist/css',
        html: 'dist',
        libs: 'dist/libs'
    },
    files: {
        js:  'app.js',
        css: 'style.css' 
    },
    server: bs
};

console.log('  --=[  requred "_conf.js"!  ]=-- ');

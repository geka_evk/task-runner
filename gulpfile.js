"use strict";

const gulp = require('gulp'),
      conf = require('./gulp/_conf'),
      fs   = require('fs');  


fs.readdirSync('./gulp')
    // .filter( file => (/\.js$/i).test(file) )
	.filter( file => !file.startsWith('_') && file.endsWith('.js') )
    .map(    file => require('./gulp/' + file) );


gulp.task('watch', () => {
   	gulp.watch( conf.src.js,   ['js']   );
   	gulp.watch( conf.src.sass, ['sass'] );
   	gulp.watch( conf.src.html, ['html'] );
});


gulp.task('default', ['js', 'sass', 'html', 'jsFiles', 'watch'], () => {
   	conf.server.init( {server: "./dist/"} );
});	


"use strict";

console.log("333"); // dsds

var i = 10;

/*
	console.log(i);
*/

i = "multiline comment: /* .... */ ";

//---------------------------------

i = 12, /*ddddddd*/console.log(i);

/* efefefef  

// */

i = 13;
'use strict';

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

var C = "CONST 10";

var i = 100,
    str = 'efefwef // wefewf';

// alert(C);

var Message = function () {
    function Message(msg) {
        _classCallCheck(this, Message);

        this.message = msg;
    }

    _createClass(Message, [{
        key: 'print',
        value: function print() {
            console.log(this.message /*, "write something"*/);
        }
    }]);

    return Message;
}();

var myMsg = new Message('Hello, how are you?????');
myMsg.print();

console.log(C); // do something very important!!

/*  fff

// ffff

*/